#!/usr/bin/env bash

set -e

build=true

case "$1" in
	"--skip-build")
		echo "Skipping build..."
		build=false
	;;
esac

if $("$build"); then
	sh build.sh
fi

projectroot=$(pwd)
builddir=/tmp/TextSuggest-bin-pkg-build

rm -rf $builddir
mkdir -p $builddir
cp bin/* $builddir/

md5sum bin/* $builddir/*

cd $builddir

mkdir -p libs

for bin in textsuggest textsuggest-server /usr/lib/qt/plugins/platforms/libqxcb.so; do
	for lib in $(ldd "$bin" | grep "=>" | awk '{ print $3 }'); do
		echo "$lib"
		cp "$lib" libs
	done;
done;

cp /usr/lib/qt/plugins/platforms/libqxcb.so libs

for bin in textsuggest textsuggest-server; do
	patchelf --set-interpreter /usr/share/textsuggest/libs/ld-linux-x86-64.so.2 --set-rpath /usr/share/textsuggest/libs/ "$bin"
done

cd $projectroot

cp -r textsuggest $builddir/textsuggest-data
cp README.md $builddir
cp LICENSE $builddir
cp bin-pkg-tools/textsuggest-server.sh $builddir
cp bin-pkg-tools/textsuggest.sh $builddir
cp bin-pkg-tools/install-bin-pkg.sh $builddir/install.sh

cd $builddir

zip -r TextSuggest-Pkg.zip * **/*

cd $projectroot

echo "Package written to $builddir/TextSuggest-Pkg.zip"